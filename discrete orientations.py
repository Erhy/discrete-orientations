# "discrete orientations.py"
# thanks for the advices and discussions in https://www.matheraum.de/
import numpy as np
# measure difference of degrees between
# arbitrary vectors and the calculated degrees from
# ( vectors in the opposite direction are treated as they were the same )
def vectArcTanDegreeDiff(vect, degr) :

	degrsToCompare = np.empty( (2,2) ) # two sets of degrees to be compared
	degrsToCompare[0,0] = np.degrees( np.arctan2( vect[0], vect[1] ) )

	vectMirrored = vect * -1 # vectors in the opposite direction
	degrsToCompare[1,0] = np.degrees(np.arctan2(vectMirrored[0], vectMirrored[1]))
	
	degrsToCompare[0,1] = degrsToCompare[1,1] = degr # calculated degree

	degrsToCompare[ degrsToCompare < 0 ] += 360 # to one between 0 and 360 degrees

	degrdiffs = np.zeros( (2) ) # differences belonging to the two sets
	for i in range(2) :
		degrdiffs[i] = np.abs( degrsToCompare[i,0] - degrsToCompare[i,1] )
		if degrdiffs[i] > 180 :
			degrdiffs[i] = 360 - degrdiffs[i]

	# note whether the difference is taken from the vector in opposite direction
	isMirrored = degrdiffs[1] < degrdiffs[0]

	return np.min(degrdiffs), isMirrored

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# verification of the calculation of degrees from arbitrary vectors
# with the method of using discrete steps of degrees
# because the subject of directional blur filter
#  results are the same for vectors in the opposite direction
#   e.g vectors ( -3, 5 ) and ( 3, -5 )  make the same

degsAround = [-90., 90.]

degsDiffApprox = 30. # approximately
# degsDiff should be a integral part of 90 degree
degsDiff = degsAround[1] / np.round( degsAround[1] / degsDiffApprox )

degTanVals = [] # list of degrees with their tangents
# e.g.: for deg in np.arange( -60., 90. - 0.01 , 30. )
# generic :
for deg in np.arange( degsAround[0] + degsDiff, degsAround[1] - 0.01 , degsDiff ) :
	# tangents are corrected to be 1 at 45 degree
	degTanVals.append( [deg, (np.tan(np.radians(deg))) * ( 1. + np.finfo(np.float64).eps ) ] )

# from list of lists extract 2 numpy arrays ( https://stackoverflow.com/questions/36436425/slicing-list-of-lists-in-python )
degVarr = np.asarray([li[0] for li in degTanVals])
tanVarr = np.asarray([li[1] for li in degTanVals])

degMidDiff = degsDiff / 2. # difference of middle degree value to values in degVarr

# routines, which want to use the results of this method get following values:
degsForFilter = np.empty((degVarr.shape[0]+1))
degsForFilter[0] = degMidDiff + degsAround[0]
degsForFilter[1:] = degVarr + degMidDiff # each element in degVarr added by ...

# smallest and biggest result from searchsorted( tanVarr, x ) belonging tanVarr
pinMinMax = [ 0, degsForFilter.shape[0] - 1 ]


# random vectors
np.random.seed(1)  # for reproducibility
VektorsRandAnzahl = 1000
VektorsRand = \
	np.stack(	(
		np.random.randint(
			-9000, 9000, size=VektorsRandAnzahl, dtype=np.int32),
		np.random.randint(
			-9000, 9000, size=VektorsRandAnzahl, dtype=np.int32)
				), axis=-1 
		)

pins = np.zeros( ( degsForFilter.shape[0] ), dtype=np.int32 ) # to count matches for each discrete result
mirroreds = np.zeros( ( 2 ), dtype=np.int32 ) # to count, whether a vector in the opposite direction match

vDiffs = [] # all degree differences between the vectors angle and the calculated discrete angle

for j in range( VektorsRandAnzahl ) : # calculation for all generated random vectors
	fromThisVector = VektorsRand[j] # one of the random vectors

	# # # # # # # # # # # # # # # # # # # # # # # # # # # #
	# only the quotient of the vector components is used
	# to assign the discrete angel
	# # # # # # # # # # # # # # # # # # # # # # # # # # # #
	if fromThisVector[1] != 0 :
		q = fromThisVector[0] / fromThisVector[1] 
		pin = np.searchsorted(tanVarr,q)
	else :
		pin = pinMinMax[1] if fromThisVector[0] > 0 else 0
	# # # # # # # # # # # # # # # # # # # # # # # # # # # #

	degCalculated = degsForFilter[pin] # calculated angle for blur-filter

	pins[pin] += 1 # count matches for each discrete result

	# determine the inaccuracy
	degdiff, isMirrored = vectArcTanDegreeDiff( fromThisVector, degCalculated )
	
	vDiffs.append( degdiff)
	if (isMirrored) :
		mirroreds[0] += 1
	else :
		mirroreds[1] += 1

	# for each random vector

# evaluation and statistics

vDiffMax = np.max( vDiffs )
vDiffMin = np.min( vDiffs )
vDiffAvr = np.average( vDiffs )
print( "points of support: each " + str(degsDiff) + " degrees" )
print( "count of random vectors: " + str(vDiffs.__len__()) )
print( "maximal deviation: " + str(vDiffMax) + " degrees" )
print( "average deviation: " + str(vDiffAvr) + " degrees" )
print( "calculations of discrete degrees and verification done" )
